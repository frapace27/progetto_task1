/**
 * TASK:
 *
 * Creare un piccolo sistema di gestione bacheca annunci, ogni annuncio sarà caratterizzato da:
 * - Nome dell'annuncio
 * - Prezzo dell'annuncio
 * - Descrizione dell'annuncio
 * - Foto del annuncio
 * - Data di pubblicazione dell'annuncio
 * - Utente che ha pubblicato l'annuncio
 *
 * 1. Creare un sistema di navigazione che ci porti tra le varie funzionalità:
 * HOME - Elenco prodotti
 * NUOVO PRODOTTO - Inserimento nuovo prodotto
 * REGISTRAZIONE - Nuovo utente
 * LOGIN - Login utente
 *
 * 2. Creare un sistema di CR-UD degli annunci (P.S. inventiamo qualcosa per Update e Delete che può essere fatto anche dopo!)
 * 3. Creare un sistema di Registrazione e Login dell'utente
 * 4. Obbligare il login prima dell'inserimento annuncio!
*/


const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const path = require('path')
const port = 4000
const fileUpload= require('express-fileupload')
const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))
app.use(fileUpload())
app.set('view engine', 'ejs')
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

//Connessione al DB
mongoose.connect(`mongodb+srv://tottigoal:tottigoal@cluster0.oy5rr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
})

//ROTTE
app.get("/", (req,res)=>{
    res.render('home')
})
app.get("/product", (req,res)=>{
    res.render('product')
})
app.get("/register", (req,res)=>{
    res.render('register')
})
app.get("/login", (req,res)=>{
    res.render('login')
})
app.post("/save", (req,res)=>{
    res.json(req.body)

    //res.render('save'
})